
Simple programm that implement CRUD operations 
=============================


## How it`s use?

When the programm starts, u will see the main page with 2 fields & buttons.
On the main page implements 2 operations:

* **create** new user;
  U may click Add Client, input email in field & click Add Clien again. 
  Or input email & click Find (user will be created if record with such email doesn`t exist)

* **find** exist user ('read' operaiton);
  Input email into email field & click find (user will be created if record with such email doesn't exist)
  Or input id into id field & click find (programm will alert u if record with such id doesn`t exist)

After user was finded/created, will be open another page where the ohter operations were inplemented:

* **create** car data;
  U may click New Car or don`t do it, then input name of model & VIN. After click submit.

* **read** car data;
  Programm collect all cars selected user into combobox. U can select car from them.

* **update** car data;
  U can change name of model, but not VIN (because it`s uniq 'number') & click submit.

* **delete** car data;
  Select car into combobox & click delete.

* **update** user data;
  U can change email address but not id. Change email into email field & click ok.

* **delete** user.
  Click delete Client.