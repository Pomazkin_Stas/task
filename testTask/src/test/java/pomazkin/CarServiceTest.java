package pomazkin;

import org.junit.jupiter.api.Test;
import pomazkin.services.CarService;
import pomazkin.services.UserService;
import pomazkin.classes.Car;
import pomazkin.classes.User;

class CarServiceTest {
    CarService carService = new CarService();
    UserService userService = new UserService();

    @Test
    void add() {
        Car car = carService.add(new Car("Ferrary", "13341k541ks4dk54d",userService.get(1)));
        carService.delete(car.getId());
    }

    @Test
    void delete() {
        Car car = carService.add(new Car("Ferrary", "133411341ks4dk54d",userService.get(1)));
        carService.delete(car.getId());
    }

    @Test
    void get() {
        Car car = carService.add(new Car("Ferrary", "13341k54qqq4dk54d",userService.get(1)));
        carService.get(car.getId());
        carService.delete(car.getId());
    }

    @Test
    void update() {
        Car car = carService.add(new Car("Ferrary", "33341k541ks4dk54d",userService.get(1)));
        carService.update(car);
        carService.delete(car.getId());
    }

    @Test
    void getAll() {
        carService.getAll();
    }

    @Test
    void getAllCarsClient() {
        User user = userService.add(new User("example1@mail"));
        carService.getAllCarsClient(user.getId());
        userService.delete(user.getId());
    }
}