package pomazkin;

import org.junit.jupiter.api.Test;
import pomazkin.services.UserService;
import pomazkin.classes.User;

class UserServiceTest {
    UserService userService = new UserService();

    @Test
    void add() {
        User user = userService.add(new User("example@mail.ru"));
        userService.delete(user.getId());
    }

    @Test
    void delete() {
        User user = userService.add(new User("example@mail.ru"));
        userService.delete(user.getId());
    }

    @Test
    void get() {
        User user = userService.add(new User("example@mail.ru"));
        userService.get(user.getId());
        userService.delete(user.getId());
    }

    @Test
    void get1() {
        User user = userService.add(new User("example@mail.ru"));
        userService.get(user.getEmail());
        userService.delete(user.getId());
    }

    @Test
    void update() {
        User user = userService.add(new User("example@mail.ru"));
        userService.update(user);
        userService.delete(user.getId());
    }

    @Test
    void getAllClients() {
        User user = userService.add(new User("example@mail.ru"));
        userService.getAllClients();
        userService.delete(user.getId());
    }
}