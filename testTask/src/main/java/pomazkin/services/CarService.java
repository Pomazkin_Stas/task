package pomazkin.services;

import pomazkin.classes.Car;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class CarService {
    public EntityManager em = Persistence.createEntityManagerFactory("Service").createEntityManager();

    public Car add(Car car) {
        em.getTransaction().begin();
        em.persist(car);
        em.getTransaction().commit();
        return car;
    }

    public void delete(long id) {
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public Car get(long id) {
        return em.find(Car.class, id);
    }

    public void update(Car car) {
        em.getTransaction().begin();
        em.merge(car);
        em.getTransaction().commit();
    }

    public List<Car> getAll() {
        TypedQuery<Car> namedQuery = em.createNamedQuery("Car.getAll", Car.class);
        return namedQuery.getResultList();
    }

    public List<Car> getAllCarsClient(long userid) {
        Query namedQuery = em.createNativeQuery("SELECT * from cars where user_id =?1", Car.class);
        namedQuery.setParameter(1,userid);
        try {
            return namedQuery.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Clients car wasn`t found");
            return null;
        }
    }
}
