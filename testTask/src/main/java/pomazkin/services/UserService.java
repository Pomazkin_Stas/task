package pomazkin.services;

import pomazkin.classes.User;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class UserService {
    public EntityManager em = Persistence.createEntityManagerFactory("Service").createEntityManager();

    public User add(User user){
        em.getTransaction().begin();
        User userFromDB = em.merge(user);
        em.getTransaction().commit();
        return userFromDB;
    }
    public void delete(long id){
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
    }

    public User get(long id){
        return em.find(User.class, id);
    }
    public Object get(String email){
        Query namedQuery = em.createNativeQuery("SELECT * from user where email =?1", User.class);
        namedQuery.setParameter(1,email);
        try {
            return namedQuery.getSingleResult();
        } catch (Exception e) {
            System.out.println("User with email: "+email+ ". Wasn`t found");
            return null;
        }

    }

    public void update(User user){
        em.getTransaction().begin();
        em.merge(user);
        em.getTransaction().commit();
    }
    public List<User> getAllClients(){
        Query namedQuery = em.createNativeQuery("SELECT * from user ", User.class);
        return namedQuery.getResultList();
    }

}
