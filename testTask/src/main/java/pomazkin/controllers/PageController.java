package pomazkin.controllers;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.scene.input.KeyEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pomazkin.services.UserService;
import pomazkin.classes.Car;
import pomazkin.classes.User;

import static pomazkin.classes.Main.*;


public class PageController {
    @FXML
    private Button client_tab;

    @FXML
    private Button cars_tab;

    @FXML
    private Button previous_button;

    @FXML
    private Button next_button;

    @FXML
    private TextField clientEmail_field;

    @FXML
    private TextField clientId_field;

    @FXML
    private Button find_button;

    @FXML
    private Button createClient_button;

    @FXML
    void FindClient() {
        String clientId = clientId_field.getText();
        String clientEmail = clientEmail_field.getText();

        if (isNumber(clientId) && clientId.length() > 0) {
            for (User u :
                    currentListUsers) {
                if (clientId.equals(u.getId().toString())) {
                    currentUser = u;
                    clientEmail = "";
                    break;
                }
            }

        } else if (clientEmail.length() > 0) {
            for (User u :
                    currentListUsers) {
                if (clientEmail.equals(u.getEmail())) {
                    currentUser = u;
                    break;
                }
            }
        }
        if (currentUser == null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "User not found", ButtonType.OK);
            alert.setHeaderText("Yoops!");
            alert.show();
        }
        else {
            transferToFindedClientPage();
        }
    }

    void transferToFindedClientPage() {

        find_button.getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/FindedClient.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        FindedClientController fCC = loader.getController();
        fCC.getEmail_field().setText(currentUser.getEmail());
        fCC.getEmailUpdate_field().setText(currentUser.getEmail());
        fCC.getSubmitCar_button().setDisable(true);
        fCC.getLeftCharacter_label().setVisible(false);
        fCC.getId_field().setText(currentUser.getId().toString());
        fCC.getId_field().setEditable(false);
        ComboBox comboBox = fCC.getCar_combobox();
        List<String> list = new ArrayList();
        for (Car c :
                currentUser.getCars()) {
            list.add(c.getVin());
        }
        comboBox.setItems(FXCollections.observableArrayList(list));
        if (list.size() > 0) {
            comboBox.setValue(comboBox.getItems().get(0));
            fCC.getVin_field().setText(comboBox.getItems().get(0).toString());
            for (Car c :
                    currentUser.getCars()) {
                if (c.getVin() == comboBox.getItems().get(0).toString())
                    fCC.getModel_field().setText(c.getModel());
            }
        }
        stage.show();
    }

    public static boolean isNumber(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @FXML
    private AnchorPane identPane;

    @FXML
    private void createClient() {
        currentUser = null;
        identPane.setVisible(false);
        if (clientEmail_field.getText().length() > 0) {
            if ((currentUser = (User) new UserService().get(clientEmail_field.getText())) == null)
                currentUser = new UserService().add(new User(clientEmail_field.getText()));
            transferToFindedClientPage();
        }
    }

    @FXML
    void IsNumber(KeyEvent event) {
        if (!event.getCode().isDigitKey()) {
            clientId_field.deletePreviousChar();
            if (event.getText().matches("\\D+"))
                clientId_field.setText(event.getText().replaceAll("\\D+", ""));
        }
    }


}


