package pomazkin.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Iterator;

import static pomazkin.classes.Main.*;

import pomazkin.services.CarService;
import pomazkin.services.UserService;
import pomazkin.classes.Car;

public class FindedClientController {

    private Car selectedItem = new Car();

    @FXML
    private Button transferToMain_button;

    public Button getTransferToMain_button() {
        return transferToMain_button;
    }

    @FXML
    private TextField emailUpdate_field;

    public TextField getEmailUpdate_field() {
        return emailUpdate_field;
    }

    @FXML
    private Button client_tab;

    @FXML
    private Button cars_tab;

    @FXML
    private Button previous_button;

    @FXML
    private Button next_button;

    public Label getEmail_field() {
        return email_field;
    }

    public void setEmail_field(Label email_field) {
        this.email_field = email_field;
    }

    @FXML
    private Label email_field;

    @FXML
    private ComboBox<String> car_combobox;

    public ComboBox<String> getCar_combobox() {
        return car_combobox;
    }

    @FXML
    private TextField model_field;

    public TextField getId_field() {
        return id_field;
    }

    @FXML
    private TextField id_field;

    public TextField getModel_field() {
        return model_field;
    }

    public TextField getVin_field() {
        return vin_field;
    }

    @FXML
    private TextField vin_field;

    @FXML
    private Button deleteCar_button;

    @FXML
    private Button submitCar_button;

    public Button getSubmitCar_button() { return submitCar_button; }

    @FXML
    private Button addNewCar_button;

    @FXML
    void getCarData() {
        model_field.setDisable(false);
        vin_field.setDisable(false);
        for (Car c :
                currentUser.getCars()) {
            if (c.getVin().equals(car_combobox.getValue())) {
                vin_field.setText(c.getVin());
                model_field.setText(c.getModel());
                System.out.println(c.getModel() + " " + c.getVin());
                selectedItem = c;
            }

        }
    }

    @FXML
    private Label leftCharacter_label;

    public Label getLeftCharacter_label() {
        return leftCharacter_label;
    }

    @FXML
    void carVinController() {
        String vinField = vin_field.getText();
        if (vinField.length() > 17) {
            vin_field.deletePreviousChar();
            vin_field.setText(vinField.substring(0, 17));
        } else if (vinField.length() == 17) {
            leftCharacter_label.setVisible(false);
            submitCar_button.setDisable(false);
        } else if (vinField.length() < 17) {
            leftCharacter_label.setVisible(true);
            leftCharacter_label.setText((17 - vinField.length()) + " characters left");
            submitCar_button.setDisable(true);
        }
        carDataISChange();
    }

    void carDataISChange() {
        if (model_field.getText().equals(selectedItem.getModel()) &&
                vin_field.getText().equals(selectedItem.getVin())) {
            submitCar_button.setDisable(true);
        } else if (vin_field.getText().length() == 17) {
            submitCar_button.setDisable(false);
        }
    }


    @FXML
    void carModelController() {
        carDataISChange();
    }

    @FXML
    void clearFields() {
        model_field.setText("");
        vin_field.setText("");

        vin_field.setDisable(false);
        model_field.setDisable(false);

        selectedItem = new Car();
    }

    @FXML
    void deleteCar() {
        if (currentUser.getCars().size() > 0) {
            Iterator<Car> iterator = currentUser.getCars().iterator();
            while (iterator.hasNext()){
                Car tempCar = null;
                if((tempCar = iterator.next()).getVin().equals(car_combobox.getValue())){
                    iterator.remove();
                    new CarService().delete(tempCar.getId());
                    car_combobox.getItems().remove(tempCar.getVin());
                    if(car_combobox.getItems().size()>0)
                        car_combobox.setValue(car_combobox.getItems().get(0));
                    break;
                }
            }
            currentUser = new UserService().get(currentUser.getId());
            for (Car c :
                    currentUser.getCars()) {
                if (car_combobox.getValue().equals(c.getVin()))
                    selectedItem = c;
                break;
            }
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Car was deleted", ButtonType.OK);
            alert.setHeaderText("Car was Deleted!");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "All cars was deleted", ButtonType.OK);
            alert.setHeaderText("All cars was Deleted!");
            alert.show();
        }
    }

    @FXML
    void createUpdateCar() {
        String vinText = vin_field.getText();
        CarService carService = new CarService();
        UserService userService = new UserService();
        boolean isUpdated = false;
        currentUser = userService.get(currentUser.getId());

        for (Car c :
                currentUser.getCars()) {
            if (vinText.equals(c.getVin())) {
                c.setModel(model_field.getText());
                carService.update(c);
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Car was updated", ButtonType.OK);
                alert.setHeaderText("Car was updated!");
                alert.show();
                isUpdated = true;
                break;
            }
        }

        if (!isUpdated) {
            carService.add(new Car(model_field.getText(), vinText, currentUser));
            car_combobox.getItems().add(vinText);
            car_combobox.setValue(vinText);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Car was added", ButtonType.OK);
            alert.setHeaderText("Car was added!");
            alert.show();

        }
        currentUser = new UserService().get(currentUser.getId());
    }

    @FXML
    void changeEmail() {
        if (emailUpdate_field.getText().length() > 0) {
            UserService userService = new UserService();
            try {
                currentUser.setEmail(emailUpdate_field.getText());
                userService.update(currentUser);
                email_field.setText(currentUser.getEmail());
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Email was updated", ButtonType.OK);
                alert.setHeaderText("Email was updated!");
                alert.show();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Try to change email address", ButtonType.OK);
                alert.setHeaderText("Email wasn't updated!");
                alert.show();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Try to change email address", ButtonType.OK);
            alert.setHeaderText("Too short email!");
            alert.show();
            emailUpdate_field.setText(currentUser.getEmail());
        }
    }

    @FXML
    void deleteClient() {
        UserService userService = new UserService();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Client was deleted", ButtonType.OK);
        alert.setHeaderText("Client was deleted!");
        alert.show();
        userService.delete(currentUser.getId());
        //transfer to startPage
        currentListUsers = userService.getAllClients();
        currentUser = null;
        transferToMain();

    }

    @FXML
    void transferToMain() {
        Thread t = new Thread(() -> {
            currentListUsers.addAll(new UserService().getAllClients());
        });
        t.start();
        currentUser = null;
        ((Stage) deleteCar_button.getScene().getWindow()).close();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/mainPage.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }
}
