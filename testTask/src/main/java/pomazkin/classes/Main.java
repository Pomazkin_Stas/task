package pomazkin.classes;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pomazkin.controllers.PageController;
import pomazkin.services.UserService;

import java.util.ArrayList;
import java.util.List;

public class Main extends Application {
    public static User currentUser = null;
    public static List<User> currentListUsers = new ArrayList<>();

    @Override
    public void start(Stage stage) throws Exception {
        UserService user = new UserService();
        Thread t = new Thread(() -> {
            currentListUsers.addAll(user.getAllClients());
        });
        t.start();

        FXMLLoader loader = new FXMLLoader();
        Parent root = (Parent) loader.load(getClass().getResourceAsStream("/fxml/mainPage.fxml"));
        PageController pController = loader.getController();
        stage.setTitle("Service");
        stage.setScene(new Scene(root));
        stage.setResizable(false);

        stage.show();
    }

    public static void main(String[] args) throws Exception {
        launch(args);
    }

}

