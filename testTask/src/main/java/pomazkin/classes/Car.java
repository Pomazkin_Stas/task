package pomazkin.classes;

import javax.persistence.*;

@Entity
@Table(name = "cars")
@NamedQuery(name = "Car.getAll", query = "SELECT c from Car c")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String model;
    @Column(unique = true)
    private String vin;
    @ManyToOne
    private User user = new User();

    public Car() {
    }

    public Car(String model, String vin, User user) {
        this.model = model;
        this.vin = vin;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVin() {
        return vin;
    }

}